export default {
  
  // product ids
  appleId: 1,
  breadId: 2,
  soupId: 4,


  /**
   * formats an amount to display as currency.
   * @param {number} amount - the amount of currency.
   * @returns {string} the amount formatted as currency.
   */
  formatAmount(amount) {
    return amount < 1 ?
      `${amount.toFixed(2)*100}p` :
      `£${amount.toFixed(2)}`
  },

  /**
   * calculates the discounts on bread for the current basket.
   * @param {object} basketProducts - the products currently in the basket,
   *                                  in the format {productId: productQuantity}
   * @param {object} productData    - data on products, in the format {productId: productData }
   * @returns {object} data on the apple discounts for this basket, objects 
   *                   which takes the format {
   *                     amount: number,
   *                     description: string
   *                   }
   */
  calculateBreadDiscount({ basketProducts, productData }) {
    let breadProduct = productData[this.breadId]

    let requiredSoup = 2

    let breadDiscount = 0.5

    let soupAmount = basketProducts[this.soupId]
    let breadAmount = basketProducts[this.breadId]

    let discountedBread = Math.min(
      Math.floor(soupAmount / requiredSoup),
      breadAmount
    )

    let totalDiscount = (breadDiscount * breadProduct.price) * discountedBread

    return {
      amount: totalDiscount,
      description: `
        ${breadDiscount * 100}% off ${discountedBread} bread with ${soupAmount} soup: -${this.formatAmount(totalDiscount)}
      `
    }
  },


  /**
   * calculates the discounts on apples for the current basket.
   * @param {object} basketProducts - the products currently in the basket,
   *                                  in the format {productId: productQuantity}
   * @param {object} productData    - data on products, in the format {productId: productData }
   * @returns {object} data on the apple discounts for this basket, objects 
   *                   which takes the format {
   *                     amount: number,
   *                     description: string
   *                   }
   */
  calculateAppleDiscount({ basketProducts, productData }) {

    let appleProduct = productData[this.appleId]

    let appleDiscount = 0.1;

    let discountedAmount = (appleProduct.price * basketProducts[this.appleId]) * appleDiscount

    let discountDescription = `
      Apples ${appleDiscount * 100}% off: -${this.formatAmount(discountedAmount)}
    `

    return {
      amount: discountedAmount,
      description: discountDescription,
    }
  },

  /**
   * calculates the discounts for the products provided.
   * @param {object} basketProducts - the products currently in the basket,
   *                                  in the format {productId: productQuantity}
   * @param {object} productData    - data on products, in the format {productId: productData }
   * @returns {object} data on the discounts for this basket, as an array of objects which
   *                   take the format {
   *                     amount: number,
   *                     description: string
   *                   }
   */
  calculateBasketDiscounts({ basketProducts, productData }) {
    let passingData = {basketProducts, productData}

    let appleDiscount = this.calculateAppleDiscount(passingData)
    let breadDiscount = this.calculateBreadDiscount(passingData)

    return [
      appleDiscount,
      breadDiscount
    ]
  },
}
