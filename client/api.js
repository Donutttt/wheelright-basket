import axios from 'axios'

export default {
  test() {
    console.log('test from api');
  },

 /**
  * gets data on all products
  * @returns {promise} a promise of product data
  */
  getProducts() {
    return axios.get(
      "https://s3.eu-west-2.amazonaws.com/wheelright-basket/products.json"
    )
  },


 /**
  * gets data on all discounts 
  * @returns {promise} a promise of discount data
  */
  getDiscounts() {
    return axios.get(
      "https://s3.eu-west-2.amazonaws.com/wheelright-basket/discounts.json"
    )
  }
}
