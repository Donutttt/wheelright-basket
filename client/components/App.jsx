import React from 'react'

import Basket from './Basket.jsx'

export default class App extends React.Component {
  render() {
    return (
      <div>
        <Basket />
      </div>
    );
  }
}
