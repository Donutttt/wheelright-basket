import React from 'react'
import { Grid, Row, Col } from 'react-bootstrap'


export default class Discount extends React.Component {

  constructor(props) {
    super(props)
  }

  render() {
    let self = this

    if (self.props.discount.amount > 0) {
      return (
        <div>
          <p style={{textAlign: "right"}}>{self.props.discount.description || ''}</p>
        </div>
      )
    } else {
      return <p></p>
    }
  }

}
