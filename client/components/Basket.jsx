import React from 'react'
import _ from 'lodash'
import { Grid, Row, Col } from 'react-bootstrap'

import api from '../api.js'
import discounter from '../simple_discounter.js'

import BasketItem from './BasketItem.jsx'
import Discount from './Discount.jsx'

const initialState = {
  // product data keyed by product id
  productsAvailable: {},

  // current product quantity keyed by product id
  currentProducts: {

  },

  // current applicable discounts
  currentDiscounts: [

  ],

  // the total cost of items in the basket
  subtotal: 0,

  // the total minus discounts
  total: 0
}

export default class Basket extends React.Component {

  constructor(props) {
    super(props)

    this.state = initialState

    // ensure methods are called with the correct this
    this.loadProductsAvailable = this.loadProductsAvailable.bind(this)
    this.handleQuantityUpdate =  this.handleQuantityUpdate.bind(this)
    this.updateBasketSubtotal = this.updateBasketSubtotal.bind(this)
    this.updateCurrentDiscounts = this.updateCurrentDiscounts.bind(this)
    this.updateBasketTotal = this.updateBasketTotal.bind(this)

    this.loadProductsAvailable()
  }


 /**
  * load into state the products currently available.
  * @returns {promise} resolves when api call finishes.
  */
  loadProductsAvailable() {
    let self = this

    return api.getProducts().then(
      data => {
        self.setState({
          productsAvailable: _.keyBy(data['data']['products'], 'id')
        })

        self.initEmptyBasket()
      }
    )
  }


 /**
  * inits currentProducts so that each basket item has 0 quantity
  */
  initEmptyBasket() {
    this.setState(prevState => {
      return {
        currentProducts: _.mapValues(
          prevState.productsAvailable,
          () => 0 // map every key to 0
        )
      }
    })
  }


  /**
   * updates current discounts
   */
  updateCurrentDiscounts() {
    let self = this

    let discounts = discounter.calculateBasketDiscounts({
      basketProducts: self.state.currentProducts,
      productData: self.state.productsAvailable
    })

    self.setState(
      { 
        currentDiscounts: discounts
      },
      () => self.updateBasketTotal ()
    )

  }


  /**
   * updates current subtotal
   */
  updateBasketSubtotal() {
    let self = this

    self.setState(
      (prevState) => {
        let totalPrices = Object.keys(prevState.currentProducts).map((key) => {
          return prevState.currentProducts[key] * prevState.productsAvailable[key]['price']
        })

        return {
          subtotal: _.sum(totalPrices)
        }
      },
      () => self.updateCurrentDiscounts()
    )
  }


 /**
  * handles an update in quantity.
  * @param {number} productId - the id of the product to update.
  * @param {number} quantity - the new quantity of product.
  */
  handleQuantityUpdate(productId, quantity) {
    console.log(`test: ${productId} ||| ${quantity}`) 
    let self = this

    self.setState(
      (prevState) => {
        let tempState = prevState

        tempState.currentProducts[productId] = quantity

        return {
          currentProducts: tempState.currentProducts
        }
      },
      () => {
        self.updateBasketSubtotal()
      }
    )
  }

  
 /**
  * updates the current total
  */
  updateBasketTotal() {
    let self = this
    let totalDiscount = self.calculateTotalDiscount()

    self.setState((prevState) => ({
      total: prevState.subtotal - totalDiscount
    }))
  }


 /**
  * renders the list of current basket items.
  * @returns {string} rendered React components.
  */
  renderCurrentBasketItems() {
    let self = this

    return Object.keys(self.state.currentProducts).map(key => {
      let thisProd = self.state.productsAvailable[key]

      return <BasketItem product={thisProd} updateQuantity={self.handleQuantityUpdate} />
    })
  }

  /**
   * returns the total amount of current discounts.
   * @return {number} total amount of discount
   */
  calculateTotalDiscount() {
    return _(this.state.currentDiscounts)
      .mapValues(value => value.amount)
      .values()
      .sum()
   }

  renderCurrentDiscounts() {
    let self = this
    
    if (self.calculateTotalDiscount() > 0) {
      return self.state.currentDiscounts.map(discount => {
        return <Discount discount={discount} />
      })
    } else {
      return <p>(No offers available)</p>
    }
  }

  render() {
    let self = this

    return (
      <div>
         <Col xs={12} md={8}>
           <h2>Basket</h2>
           <Row>
             { self.renderCurrentBasketItems() }
           </Row>
           <Row>
             <div class="pull-right">
               <p>Subtotal: £{ self.state.subtotal.toFixed(2) }</p>
             </div>
           </Row>
           <Row>
             <div class="pull-right">
               { self.renderCurrentDiscounts() }
             </div>
           </Row>
           <Row>
             <div class="pull-right">
               Total price: £{ self.state.total.toFixed(2)}
             </div>
           </Row>
           <Row>
             <div class="btn btn-success pull-right" style={{ marginTop: "1em" }}>Proceed</div>
           </Row>
         </Col>
      </div>
    );
  }

}
