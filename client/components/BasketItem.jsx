import React from 'react'
import classnames from 'classnames'
import { Grid, Row, Col } from 'react-bootstrap'

import styles from './styles/BasketItem.css'

let imageWrapperClasses = classnames(
  'col-xs-2',
)


export default class BasketItem extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      product: props.product,
      quantity: 0,
      quantityUpdate: props.updateQuantity
    }

    this.plusClick = this.plusClick.bind(this)
    this.minusClick = this.minusClick.bind(this)
  }

  plusClick() {
    let self = this

    self.setState(
      prevState => ({
        quantity: prevState.quantity + 1
      }),
      () =>  self.state.quantityUpdate(self.state.product.id, self.state.quantity)
    )

  }

  minusClick() {
    let self = this

    this.setState(
      prevState => ({
        quantity: Math.max(prevState.quantity -1, 0)
      }),
      () =>  self.state.quantityUpdate(self.state.product.id, self.state.quantity)
    )
  }

  render() {
    let self = this

    return (
      <div className={ styles.itemWrapper  }>
        <Row>
          <Col xs={2} sm={2} xsHidden>
            <img className={ styles.productImage } src={ self.state.product.image } alt="" />
          </Col>
          <Col xs={5} sm={5}>
             <h3 style={{ paddingLeft: "1em" }}>{ self.props.product.name }</h3>
          </Col>
          <Col xs={5} sm={3} style={{ marginTop: "2em" }}>
            <span style={{ cursor: "pointer" }} onClick={ self.minusClick }><i class="glyphicon glyphicon-minus"></i></span>
            <span> { self.state.quantity } </span>
            <span style={{ cursor: "pointer" }} onClick={ self.plusClick }><i class="glyphicon glyphicon-plus"></i></span>
          </Col>
          <Col xs={2} sm={2} style={{ marginTop: "2em" }}>
            £{ (self.state.product.price * self.state.quantity).toFixed(2) }
          </Col>
       </Row>
      </div>
    );
  }

}
